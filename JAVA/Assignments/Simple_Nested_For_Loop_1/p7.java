/*
 
   Q7 write a program to print the following pattern

   	D D D D
	D D D D
	D D D D
	D D D D

*/

class For{

        public static void main(String[] args){
                int rows = 4;
		
                for(int i = 0;i<rows;i++){
                        for(int j = 0; j<rows;j++){
                                System.out.print("D ");
                        }
                        System.out.println();
                }
        }
}
