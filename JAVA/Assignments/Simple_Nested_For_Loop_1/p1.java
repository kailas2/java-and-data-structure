/*
 
   Q1 write a program to print the following pattern

   	C2W C2W C2W
	C2W C2W C2W
	C2W C2W C2W

*/

class For{

	public static void main(String[] args){
		int rows = 3;

		for(int i = 0;i<rows;i++){
			for(int j = 0; j<rows;j++){
				System.out.print("C2W ");
			}
			System.out.println();
		}
	}
}

