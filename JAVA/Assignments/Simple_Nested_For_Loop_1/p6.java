/*
  
   Q6 write a program to print the following pattern

   	9 8 7
	9 8 7
	9 8 7

	USE THIS FOR LOOP STRICTLY

	for(int i =1;i<=3;i++){
		for(j=1;j<=3;j++){
		}
	}
  
*/

class For{

        public static void main(String[] args){
                int rows = 3;
		int no = rows*rows;

                for(int i = 1;i<=rows;i++){
                        for(int j = 1; j<=rows;j++){
                                System.out.print(no - j + 1 + " ");
                        }
                        System.out.println();
                }
        }
}
