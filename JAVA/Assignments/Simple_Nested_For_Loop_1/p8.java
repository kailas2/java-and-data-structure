/*
 
  Q8  write a program to print the following pattern

	d d d d
	c c c c
	b b b b
	a a a a

*/

class For{

        public static void main(String[] args){
                int rows = 4;
		char ch = (char)(96+rows);

                for(int i = 0;i<rows;i++){
                        for(int j = 0; j<rows;j++){
                                System.out.print(ch + " ");
                        }
                        System.out.println();
			ch--;
                }
        }
}
