/*
  
  Q5 write a program to print the following pattern

  	12 12 12
	11 11 11
	10 10 10
 
*/

class For{

        public static void main(String[] args){
                int rows = 3;
		int no = 12;
                for(int i = 0;i<rows;i++){
                        for(int j = 0; j<rows;j++){
                                System.out.print(no-i + " ");
                        }
                        System.out.println();
                }
        }
}
