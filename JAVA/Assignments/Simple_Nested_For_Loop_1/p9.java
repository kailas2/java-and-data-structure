/*

  Q9  write a program to print the following pattern

  	C B A
	C B A
	C B A

*/

class For{

        public static void main(String[] args){
                int rows = 3;

                for(int i = 0;i<rows;i++){
			char ch = (char)(64+rows);
                        for(int j = 0; j<rows;j++){
                                System.out.print(ch-- + " ");
                        }
                        System.out.println();
                }
        }
}
