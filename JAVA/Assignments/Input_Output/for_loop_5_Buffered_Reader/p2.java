/*
 	Take all the inputs from the user
	Use BufferedReader
	
	Q2 write a program to print the following pattern

	# = = = =
	= # = = =
	= = # = =
	= = = # =
	= = = = #

*/

import java.io.*;
class pattern{
	public static void main(String[] args)throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Rows");
		int row = Integer.parseInt(Br.readLine());
		System.out.println("Enter Col");
		int col = Integer.parseInt(Br.readLine());
		for(int i = 1;i<=row;i++){
			for(int j = 1;j<=col;j++){
				if(i == j){
					System.out.print("# ");
				}
				else{
					System.out.print("= ");
				}
			}
			System.out.println();
		}	
	}
}

