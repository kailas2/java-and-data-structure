/*
 Q6
	Write a program, and take two characters if these characters are equal then print them as it is but if
	they are unequal then print their difference.
	
	{ Note: Consider Positional Difference Not ASCIIs }
	Input: a p
	Output: The difference between a and p is 15



*/

import java.io.*;
class pattern{
	public static void main(String[] args) throws IOException{
		InputStreamReader Isr = new InputStreamReader(System.in);
		System.out.println("Enter First Character");
		char ch1 = (char) Isr.read();
		char c = (char) Isr.read();
		System.out.println("Enter Second character");

		char ch2 = (char) Isr.read();

		if(ch1 == ch2){
			System.out.println("Both Characters are Same");
		}
		else{
			if(ch1 > ch2){
				System.out.println("The difference between " + ch1 +" and " + ch2 +  " is " + (ch1-ch2));

			}	
			else{
				System.out.println("The difference between " + ch1 +" and " + ch2 +  " is " + (ch2-ch1));
			}
		}
	}
}
