/*
 
Q9
Write a program to take a number as input and print the Addition of Factorials of each
digit from that number.
Input: 1234
Output: Addition of factorials of each digit from 1234 = 33

*/

import java.io.*;
class pattern{
        public static void main(String[] args) throws IOException{
                BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter No");
                int No = Integer.parseInt(Br.readLine());
                int Addition = 0;
		int temp = No;
		while(No>0){
			int val = No%10;
			int fact = 1;
			while(val > 1){
				fact = fact * val;
				val--;
			}
			Addition = Addition + fact;
			No = No/10;
		}
		System.out.println("Addition of factorials of each digit from "+ temp + " = " + (Addition));
	}
}




