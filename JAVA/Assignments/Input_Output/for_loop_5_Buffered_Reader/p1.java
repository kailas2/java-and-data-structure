/*
 	Take all the inputs from the user
	Use BufferedReader
	
	Q1   write a program to print the following pattern
		
	D4 C3 B2 A1
	A1 B2 C3 D4
	D4 C3 B2 A1
	A1 B2 C3 D4

 */

import java.io.*;
class pattern{
	public static void main(String[] args) throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Rows");
		int row = Integer.parseInt(Br.readLine());
		System.out.println("Enter Col");
		int col = Integer.parseInt(Br.readLine());
		for(int i = 0;i<row;i++){

			for(int j = 0;j<col;j++){
				if(i%2==0){
					System.out.print((char)(64+row-j)+""+(row-j)+" ");
				}
				else{
					System.out.print((char)(65+j)+""+(j+1)+" ");
				}
			}
			System.out.println();
		}	
	}
}

