/*
 	Take all the inputs from the user
	Use BufferedReader
	
	Q8
	write a program to print the following pattern
	Row =8
	
	$
	@ @
	& & &
	# # # #
	$ $ $ $ $
	@ @ @ @ @ @
	& & & & & & &
	# # # # # # # #

*/

import java.io.*;
class pattern{
	public static void main(String[] args) throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Rows");
		int row = Integer.parseInt(Br.readLine());
		int no = 1;
		for(int i = 1;i<=row;i++){
			for(int j = 1;j<=i;j++){
				if(no == 1)
					System.out.print("$ ");
				if(no == 2)
					System.out.print("@ ");

				if(no == 3)
					System.out.print("& ");
				if(no == 4)
					System.out.print("# ");
			}
			System.out.println();
			if(no == 4){
				no = 0;
			}
			no++;

		}	
	}
}
