/*
 	Take all the inputs from the user
	Use BufferedReader
	
	Q3    write a program to print the following pattern
	
	5 4 3 2 1
	8 6 4 2
	9 6 3
	8 4
	5

 */

import java.io.*;
class pattern{
	public static void main(String[] args) throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Rows");
		int row = Integer.parseInt(Br.readLine());

		for(int i = 1;i<=row;i++){
			int val = (row-i+1)*(i+1-1);
			for(int j = 1;j<=row-i+1;j++){
				System.out.print(val + " ");
				val = val - i;
			}
			System.out.println();
		}	
	}
}
