/*
 	Take all the inputs from the user
	Use BufferedReader
	
	Q7
		write a program to print the following pattern
	Row =5;
	
	O
       14  13
	L  K  J
	9  8  7 6
	E  D  C B A


 */

import java.io.*;
class pattern{
	public static void main(String[] args)throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Rows");
		int row = Integer.parseInt(Br.readLine());
		int no = 1;
		int no2 = row;
		int no3 = 1;
		if(row%2==0){
			no = 0;
			no2 = row-1;
			no3 = 0;
		}
		int no1 = (row*(row+1))/2;
		char ch = (char)(64+no1);
		for(int i = no;i<=no2;i++){
			for(int j = no3;j<=i;j++){
				if(i%2==1){
					System.out.print(ch+ " ");
				}
				else{
					System.out.print(no1+" ");
				}
				no1--;
				ch--;
			}
			System.out.println();
		}	
	}
}
