/*
 *Q10
write a program to print a series of prime numbers from entered range. ( Take a start and end number
from a user )
Perform dry run at least from 10 to 20 …
Input:-
Enter starting number: 10
Enter ending number: 100
Output:-
Series = 11 13 17 19 ….. 89 9
*/
import java.io.*;
class pattern{
        public static void main(String[] args) throws IOException{
                BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Start");
                int Start = Integer.parseInt(Br.readLine());
                System.out.println("Enter End");
                int End = Integer.parseInt(Br.readLine());
		for(int i = Start;i<=End;i++){
			int cnt = 0;
			for(int j = 1;j<=i;j++){
				if(i%j == 0){
					cnt++;
				}
			}
			if(cnt == 2){
				System.out.print(i + " ");
			}
		}
	}
}


