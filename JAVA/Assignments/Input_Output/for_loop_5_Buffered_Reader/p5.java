/*
 	Take all the inputs from the user
	Use BufferedReader
	
	Q5   write a program to print the following pattern
	Row =4

	0
	1 1
	2 3 5
	8 13 21 34

 */

import java.io.*;
class pattern{
	public static void main(String[] args) throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Rows");
		int Row = Integer.parseInt(Br.readLine());
		int a = 0;
		int b = 1;
		int c = 1;
		for(int i = 0;i<=Row;i++){
			for(int j = 0;j<=i;j++){
				if(a == 0 || a == 1){
					System.out.print(a + " ");
				}
				else{
					System.out.print(c + " ");
				}
				b = a;
				a = c;
				c = c + b;
			}
			System.out.println();
		}	
	}
}
