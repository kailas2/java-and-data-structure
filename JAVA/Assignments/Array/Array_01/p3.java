/*
 
Program 3

WAP to take size of array from user and also take integer elements from user Print
product of odd index only
input : Enter size : 6
Enter array elements : 1 2 3 4 5 6
output : 48
//2 * 4 * 6

*/


import java.io.*;
class Array_Demo{
        public static void main(String[] args)throws IOException{
                BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size for an array");
                int size = Integer.parseInt(Br.readLine());
                int arr[] = new int[size];
                int product = 1;
                System.out.println("Enter Elements in an array");
                for(int i = 0; i < arr.length;i++){
                        arr[i] = Integer.parseInt(Br.readLine());
                        if(i%2==1)
                                product = product * arr[i];
                }
                System.out.println("Product of an odd index elememts in an array is = " + product);
        }

}

