/*
 Program 2
WAP to take size of array from user and also take integer elements from user Print
product of even elements only
input : Enter size : 9
Enter array elements : 1 2 3 2 5 10 55 77 99
output : 40
// 2 * 2 * 10
*/




import java.io.*;
class Array_Demo{
        public static void main(String[] args)throws IOException{
                BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size for an array");
                int size = Integer.parseInt(Br.readLine());
                int arr[] = new int[size];
                int Product = 1;
                System.out.println("Enter Elements in an array");
                for(int i = 0; i<arr.length;i++){
                        arr[i] = Integer.parseInt(Br.readLine());
                        if(arr[i]%2==0)
                                Product = Product * arr[i];
                }
                System.out.println("Product of an even no's in an array is = " + Product);
        }
}
