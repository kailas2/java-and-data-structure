/*
 Program 5
Write a program ,take 10 input from the user and print only elements that are divisible by
5.
Input: 10 2 2 3 3 3 4 4 25 55
Output: 10 25 5
*/



import java.io.*;
class Array_Demo{
        public static void main(String[] args)throws IOException{
                BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size for an array");
                int size = Integer.parseInt(Br.readLine());
                int arr[] = new int[size];
                int sum = 0;
                System.out.println("Enter Elements in an array");
                for(int i = 0; i<arr.length;i++){
                        arr[i] = Integer.parseInt(Br.readLine());
           	}
                for(int i = 0; i<arr.length;i++){
                        if(arr[i]%5==0){
                		System.out.print(arr[i] + " ");
			}
           	}
                System.out.println();
        }
}
