/*

Program 4

Write a program, take 7 characters as an input , Print only vowels from the array
Input: a b c o d p e
Output : a o e

*/


import java.io.*;
class Array_Demo{
        public static void main(String[] args)throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size for an array");
		int size = Integer.parseInt(Br.readLine());
                char arr[] = new char[size];
                int sum = 0;
                System.out.println("Enter Characters in an array");
                for(int i = 0; i<arr.length;i++){
			int no = Br.read();
			arr[i] = (char)(no);
			Br.skip(1);

                }
		for(int i = 0;i<arr.length;i++){
			if(arr[i] == 'A' || arr[i] =='E' || arr[i] =='I' || arr[i] =='O' || arr[i] =='U' || arr[i] =='a' || arr[i] =='e' || arr[i] =='i' || arr[i] == 'o' || arr[i] == 'u'){
				System.out.print(arr[i] + " ");
			}
		}
		System.out.println();
        }
}
