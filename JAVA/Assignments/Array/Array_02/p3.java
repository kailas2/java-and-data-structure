/*
 Program 3
Write a Java program to find the sum of even and odd numbers in an array.
Display the sum value.
Input: 11 12 13 14 15
Output
Odd numbers sum = 39
Even numbers sum = 26

*/

import java.io.*;
class Array_Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size = Integer.parseInt(Br.readLine());
		int arr[] = new int[size];
		System.out.println("Enter Array Elements");
		int OddSum = 0;
		int EvenSum = 0;
		for(int i = 0;i<arr.length;i++){
			arr[i] = Integer.parseInt(Br.readLine());
			if(arr[i]%2==0)
				EvenSum = EvenSum + arr[i];
			else
				OddSum = OddSum + arr[i];
		}
		System.out.println("Odd numbers sum = " + OddSum);
		System.out.println("Even numbers sum = " + EvenSum);

	}
}

