/*
 Program 5
WAP to take size of array from user and also take integer elements from user
find the minimum element from the array
input : Enter size : 5
Enter array elements: 1 2 5 0 4
output: min element = 0
*/

import java.io.*;
class Array_Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size = Integer.parseInt(Br.readLine());
		int arr[] = new int[size];
		System.out.println("Enter Array Elements");
		for(int i = 0;i<arr.length;i++){
			arr[i] = Integer.parseInt(Br.readLine());
		}
		int min = arr[0];
		for(int i = 0;i<arr.length;i++){
			if(arr[i] < min){
				min = arr[i];
			}
		}
		System.out.println("min element = " + min);


	}
}

