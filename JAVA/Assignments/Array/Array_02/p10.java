/*
 Program 10
WAP to print the elements whose addition of digits is even.
Ex. 26 = 2 + 6 = 8 (8 is even so print 26)
Input :
Enter array : 1 2 3 5 15 16 14 28 17 29 123
Output: 2 15 28 17 12

*/
import java.io.*;
class Array_Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size = Integer.parseInt(Br.readLine());
		int arr[] = new int[size];
		System.out.println("Enter Array Elements");
		for(int i = 0;i<arr.length;i++){
			arr[i] = Integer.parseInt(Br.readLine());
		}
		for(int i = 0;i<arr.length;i++){
			int no = arr[i];
			int sum = 0;
			while(no > 0){
				sum = sum + no%10;
				no = no/10;
			}
			if(sum%2==0){
				System.out.print(arr[i] + " ");
			}
		}
		System.out.println();

	}
}

