/*
Program 4
WAP to search a specific element from an array and return its index.
Input: 1 2 4 5 6
Enter element to search: 4
Output: element found at index: 2
*/

import java.io.*;
class Array_Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size = Integer.parseInt(Br.readLine());
		int arr[] = new int[size];
		System.out.println("Enter Array Elements");
		for(int i = 0;i<arr.length;i++){
			arr[i] = Integer.parseInt(Br.readLine());
		}
		System.out.println("Enter Search Elements");

		int Search_Element = Integer.parseInt(Br.readLine());
		int temp = 0;
		for(int i = 0;i<arr.length;i++){
			if(arr[i] == Search_Element){
				System.out.println("element found at index:"+ " " + i);
				temp = 1;
				break;
			}
		}
		if(temp == 0){
			System.out.println("Element Not Found");
		}

	}
}
