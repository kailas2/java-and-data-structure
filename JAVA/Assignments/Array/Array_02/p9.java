/*
 Program 9
Write a Java program to merge two given arrays.
Array1 = [10, 20, 30, 40, 50]
Array2 = [9, 18, 27, 36, 45]
Output :
Merged Array = [10, 20, 30, 40, 50, 9, 18, 27, 36, 45]
Hint: you can take 3rd array

*/


import java.io.*;
class Array_Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size = Integer.parseInt(Br.readLine());
		int arr1[] = new int[size];
		System.out.println("Enter Array Elements");
		for(int i = 0;i<arr1.length;i++){
			arr1[i] = Integer.parseInt(Br.readLine());
		}
		System.out.println("Enter Array Size");
		int size2 = Integer.parseInt(Br.readLine());
		int arr2[] = new int[size2];
		System.out.println("Enter Array Elements");
		for(int i = 0;i<arr2.length;i++){
			arr2[i] = Integer.parseInt(Br.readLine());
		}
		
		int size3 = size+size2;
		int arr3[] = new int[size3];
		int no = 0;
		for(int i = 0;i<arr1.length;i++){
			arr3[no] = arr1[i];
			no++;
		}
		for(int i = 0;i<arr2.length;i++){
			arr3[no] = arr2[i];
			no++;
		}
		System.out.print("arr1 = ");
		for(int i = 0;i<arr1.length;i++){
			System.out.print(arr1[i] + " ");
		}
		System.out.println();
		System.out.print("arr2 = ");

		for(int i = 0;i<arr2.length;i++){
			System.out.print(arr2[i] + " ");
		}
		System.out.println();
		System.out.print("arr3 = ");
		for(int i = 0;i<arr3.length;i++){
			System.out.print(arr3[i] + " ");
		}
		System.out.println();



	}
}

