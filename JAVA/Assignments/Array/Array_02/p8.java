/*
 Program 8
WAP to find the uncommon elements between two arrays.
Input :
Enter first array : 1 2 3 5
Enter Second array: 2 1 9 8
Output: UnCommon elements :
3
5
9
8
*/
import java.io.*;
class Array_Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size1 = Integer.parseInt(Br.readLine());
		int arr1[] = new int[size1];
		System.out.println("Enter Array Elements");
		int no = 0;
		int max = 0;
		for(int i = 0;i<arr1.length;i++){
			arr1[i] = Integer.parseInt(Br.readLine());
			if(no == 0){
				max = arr1[i];
				no++;
			}
			else{
				if(arr1[i]>max){
					max = arr1[i];
				}
			}

		}
		System.out.println("Enter Array Size");
		int size2 = Integer.parseInt(Br.readLine());
		int arr2[] = new int[size2];
		System.out.println("Enter Array Elements");
		for(int i = 0;i<arr2.length;i++){
			arr2[i] = Integer.parseInt(Br.readLine());
			if(arr2[i] > max){
				max = arr2[i];
			}
		}
		int arr3[] = new int[max+1];
		for(int i = 0;i<arr1.length;i++){
			arr3[arr1[i]]++;
		}
		for(int i = 0;i<arr2.length;i++){
			arr3[arr2[i]]++;
		}
		System.out.print("UnCommom Elements = ");
		for(int i = 0;i<arr1.length;i++){
			if(arr3[arr1[i]] == 1){
				System.out.print(arr1[i] + " ");
				arr3[arr1[i]] = 0;
			}
		}
		for(int i = 0;i<arr2.length;i++){
			if(arr3[arr2[i]] == 1){
				System.out.print(arr2[i] + " ");
				arr3[arr2[i]] = 0;
			}
		}
		System.out.println();

	}
}

