/*
 
Program 1
Write a program to print count of digits in elements of array.
Input: Enter array elements : 02 255 2 1554
Output: 2 3 1 4

*/

import java.io.*;
class ArrayDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Size for an array");
		int size = Integer.parseInt(Br.readLine());
		int arr[] = new int[size];

		System.out.println("Enter elements in an array");
		for(int i = 0;i<arr.length;i++){
			arr[i] = Integer.parseInt(Br.readLine());
		}
		for(int i = 0;i<arr.length;i++){
			int no = arr[i];
			int cnt = 0;
			while(no > 0){
				cnt++;
				no = no/10;
			}
			System.out.print(cnt + " ");
		}
		System.out.println();

	}
}
