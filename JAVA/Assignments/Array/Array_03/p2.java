/*
 Program 2
WAP to reverse each element in an array.
Take size and elements from the user
Input: 10 25 252 36 564
Output: 01 52 252 63 465

*/
import java.io.*;
class ArrayDemo{
        public static void main(String[] args)throws IOException{
                BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Size for an array");
                int size = Integer.parseInt(Br.readLine());
                int arr[] = new int[size];

                System.out.println("Enter elements in an array");
                for(int i = 0;i<arr.length;i++){
                        arr[i] = Integer.parseInt(Br.readLine());
                }
                for(int i = 0;i<arr.length;i++){
                        int no = arr[i];
                        int rev = 0;
			int cnt = 0;
                        while(no > 0){
                                cnt++;
                                no = no/10;
                        }
			no = arr[i];

			while(no > 0){
				rev = no%10 + (rev*10);
                                no = no/10;
                        }
			int rev1 = rev;
			int cnt1 = 0;
			while(rev1 > 0){
				cnt1++;
                                rev1 =  rev1/10;
                        }

			if(cnt == cnt1)
                        	System.out.print(rev + " ");
			else{
				while(cnt1 != cnt){
                        		System.out.print("0");
					cnt1++;
				}
                        	System.out.print(rev + " ");
			}
                }
                System.out.println();

        }
}
