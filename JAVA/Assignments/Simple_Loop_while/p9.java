/*
		
   Program 9: Write a program to reverse the given number.

	Input: 942111423
	
	Output: 324111249

*/


class Demo {

	public static void main(String args[]){

		long num = 702829;

		long Rev = 0;
		while(num != 0){
			Rev = Rev * 10 + num % 10;

			num = num/10;
		}
		System.out.println(Rev);
	}
}
			
