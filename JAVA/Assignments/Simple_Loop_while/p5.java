//square of even number


class SqOfEvenNo {

	public static void main(String args[]){

		long num = 942111423;

		while(num!=0){
			
			long x = num%10;

			if(x%2==0){
				System.out.println(x*x);
			}
			num=num/10;
		}
	}
}
