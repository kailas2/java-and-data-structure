/*
 
  Program 7: Write a program which take’s number from user’s if number is even
print that number in reverse order or if number is odd print that number in
reverse order by difference two?
Input:6
output:6 5 4 3 2 1
Input:7
output:7 5 3 1.

*/


class Demo {
	public static void main(String args[]){

		int num = 9;

		if(num%2==0){
			while(num>0){
				System.out.println(num--);
			}
		}else{
			while(num>0){
				System.out.println(num);
				num=num-2;
			}
		}

	}
}

