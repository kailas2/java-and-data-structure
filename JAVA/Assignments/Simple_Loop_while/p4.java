/*
 
   Program 4: Write a program to count the Odd digits of the given number.

	Input: 942111423

	Output: count of odd digits = 5

*/

class OddCnt {

	public static void main(String args[]){

		int count = 0;
		long num=942111423;

		while(num != 0){

			long x = num%10;

			if(x%2 == 1){
				count++;
			}
			num = num/10;
		}
		System.out.println("count of odd digits = "+count);
	}
}
