/*
 
   Program 2: Write a program to calculate the factorial of the given number.
	
	Input: 6
	
	Output: factorial 6 is 720

*/


class factorial {
	public static void main(String args[]){

		int fact=1;
		
		int num=5;

		while(num>0){
			
			fact = fact*num--;
		}
		System.out.println(fact);
	}
}
