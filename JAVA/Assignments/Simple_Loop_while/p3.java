/*
 
   Program 3: Write a program to count the digits of the given number.
	
	input: 942111423
	
	Output: count of digits = 9

*/


class DigitCnt {
	public static void main(String args[]){

		long num = 942011023;

		int count=0;

		while(num != 0){
			long x = num % 10;
			num = num/10;
			count++;
		}
		System.out.println("count of digits = "+count);
	}
}
