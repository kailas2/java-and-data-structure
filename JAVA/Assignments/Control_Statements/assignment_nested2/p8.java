/*
 Q8
write a program to print the following pattern
A b C d
E f G h
I j K l
M n O p
*/

class pattern{
	public static void main(String[] args){
		int row = 4;
		int col = 4;
		char ch = 65;
		char ch1 = 97;
		int num = 0;
		for(int i = 1;i<=row;i++){
			if(i%2==1)
				System.out.print(ch+" ");
			else
				System.out.print(ch1+" ");
			if(i == col){
				System.out.println();
				i = 0;
				num++;
			}
			if(num == row){
				break;
			}
			ch++;
			ch1++;

		}
	}
}
