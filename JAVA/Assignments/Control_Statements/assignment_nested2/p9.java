/*
 Q9
write a program to print the following pattern
1C3 4B2 9A1
16C3 25B2 36A1
49C3 64B2 81A1
*/

class pattern{
	public static void main(String[] args){
		int row = 3;
		int col = 3;
		int no = 3;
		int n = 1;
		for(int i = 1;i<=row;i++){
			System.out.print(n*n +""+(char)(64+no)+""+ no-- +" ");

			if(i == col){
				System.out.println();
				i = 0;
				no = 3;
			}
			if(n == row*col)
				break;
			n++;


		}
	}
}

