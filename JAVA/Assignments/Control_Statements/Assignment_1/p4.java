/* Program 4: Write a java program that checks a number from 0 to 5 
   and prints its spelling, if the number is greater than 5 print the number is greater than 5
*/

class Spelling{
	public static void main(String[] args){
		int num = 1;
		if (num > 5)
			System.out.println("Greater than 5");
		else if(num == 1)
			System.out.println("One");
		else if(num == 2)
                        System.out.println("Two");
		else if(num == 3)
                        System.out.println("Three");
		else if(num == 4)
                        System.out.println("Four");
		else if(num == 5)
                        System.out.println("Five");
		else
			System.out.println("Invalid Input");
	}
}

