/*
 
   Q3  write a program to print the following pattern

   	14 15 16 17
	15 16 17 18
	16 17 18 19
	17 18 19 20
	
	USE THIS FOR LOOP STRICTLY

	for(int i =1;i<=4;i++){
		for(j=1;j<=4;j++){
		}
	}

*/

class For{

        public static void main(String[] args){
                int rows = 4;
		int no = 14;
                for(int i = 1;i<=rows;i++){
                        for(int j = 1; j<=rows;j++){
                                System.out.print(no+j-1+" ");
                        }
                        System.out.println();
			no++;
                }
        }
}
