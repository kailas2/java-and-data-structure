/*
 
  Q1 write a program to print the following pattern

  	C2W1 C2W2 C2W3
	C2W1 C2W2 C2W3
	C2W1 C2W2 C2W3

*/

class For{

        public static void main(String[] args){
                int rows = 3;

                for(int i = 0;i<rows;i++){
                        for(int j = 1; j<=rows;j++){
                                System.out.print("C2W"+j+" ");
                        }
                        System.out.println();
                }
        }
}
