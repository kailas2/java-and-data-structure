/*

  Q10  write a program to print the following pattern

	F 5 D 3 B 1
	F 5 D 3 B 1
	F 5 D 3 B 1
	F 5 D 3 B 1
	F 5 D 3 B 1
	F 5 D 3 B 1

	USE THIS FOR LOOP STRICTLY

	for(int i =1;i<=6;i++){
		for(j=1;j<=6;j++){
		}
	}

*/

class For{

        public static void main(String[] args){
                int rows = 6;
		
                for(int i = 1;i<=rows;i++){
                        for(int j = 1; j<=rows;j++){
                                if(j%2==1){
					System.out.print((char)(64+rows-j+1)+" ");
				}
				else{
				
					System.out.print(rows-j+1+" ");
				}
                        }
                        System.out.println();
                }
        }
}
