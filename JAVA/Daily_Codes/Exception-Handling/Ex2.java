class Demo{
	void m1(){
		System.out.println(10/0);
		System.out.println("in m1");
		m2();
	}
	void m2(){
		System.out.println("in m1");
	}
	public static void main(String[] args){
		Demo obj = new Demo();
		obj.m1();
	}

}

// Runtime Exception

/*
 
Exception in thread "main" java.lang.ArithmeticException: / by zero
        at Demo.m1(Ex2.java:3)
        at Demo.main(Ex2.java:12) 
 
*/


/*
 
   flow of exception by default exception handler:

   	1) Thread Name
	2) Exception Name
	3) Discription
	4) Stack Trace
*/
