import java.util.*;
class Demo{
	public static void main(String[] args){
		Scanner Sc = new Scanner(System.in);
		System.out.println("Enter VAlue For X");
		int x = Sc.nextInt();
		try{
			if(x == 0){
				throw new ArithmeticException("Divide By Zero");
			}
			System.out.println(10/x);
		}
		catch(ArithmeticException AE){
			System.out.println("Exception in thread "+ Thread.currentThread().getName()+" ");
			AE.printStackTrace();
		}
	}
}
