// finally block


class Demo{
	void m1(){
	
	}
	void m2(){
	
	}
	public static void main(String[] args){
		Demo obj = new Demo();
		obj.m1();
		obj = null;
		try{
			obj.m2();
		}
		catch(NullPointerException npe){
			System.out.println("Exception catched here");
		}
		finally{
			System.out.println("Connection closed");
		}
		System.out.println("End main");
		
	}
}

