import java.io.*;

class ExceptionDemo{
	public static void main(String[] args)throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		String Str = Br.readLine();
		System.out.println(Str);
		int Data = Integer.parseInt(Br.readLine());
		System.out.println(Data);

	}
}

// When You Enter String For Data Input It gives NumberFormatException:

/*

Exception in thread "main" java.lang.NumberFormatException: For input string: "kailas"
        at java.base/java.lang.NumberFormatException.forInputString(NumberFormatException.java:67)
        at java.base/java.lang.Integer.parseInt(Integer.java:668)
        at java.base/java.lang.Integer.parseInt(Integer.java:784)
        at ExceptionDemo.main(Ex6.java:8)

*/

