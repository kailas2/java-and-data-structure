import java.util.Scanner;

class HeavySoundException extends RuntimeException{
	HeavySoundException(String Msg){
		super(Msg);
	}
}
class Demo {
	
	public static void main(String[] args){
		Scanner Sc = new Scanner(System.in);
		System.out.println("Enter Sound Level");
		int SoundLevel = Sc.nextInt();
		if(SoundLevel >= 90){
			throw new HeavySoundException("Please Keep Your Sound Level Below 90db , Sound More than 90db is Not good for Hearing");
		}
		System.out.println("Enjoy Music");
	}
}

