import java.io.*;
class ExceptionDemo{
	public static void main(String[] args)throws IOException,NumberFormatException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		int Data = 0;
		try{
			Data = Integer.parseInt(Br.readLine());
		}
		catch(NumberFormatException obj){
			System.out.println("Please Enter Integer");
			Data = Integer.parseInt(Br.readLine());
		}
		System.out.println(Data);
	}
}
