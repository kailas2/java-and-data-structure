class outer{
	void m1(){
		System.out.println("in m1 outer");
		class inner{
			void m2(){
				System.out.println("In m1 inner");
			}
		}
		inner obj1 = new inner();
		obj1.m2();
	}
	void m2(){
		System.out.println("in m2 outer");

	}
}
class client{
	public static void main(String[] args){
		outer obj = new outer();
		obj.m1();
		obj.m2();
	}
}
