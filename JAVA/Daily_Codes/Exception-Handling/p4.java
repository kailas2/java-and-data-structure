class outer{a
	Object m1(){
		System.out.println("in outer");
		class inner{
			void m2(){
				System.out.println("in inner");
			}
		}
		return new inner;

	}
}
class client{
	public static void main(String[] args){
		outer obj = new outer();
		obj.m1().m2();
	}

}

