class outer{
	Object m1(){
		System.out.println("in m1 outer");
		class inner{
			void innerm1(){
				System.out.println("in m1 inner");
			}
		}
		return new inner();
		
	}
	void m2(){
		System.out.println("in m2 outer");
	}
}
class client{
	public static void main(String[] args){
		outer obj = new outer();
		obj.m1();
		obj.m2();
	}
}

