import java.util.Scanner;
class DataOverFlowException extends RuntimeException{
	DataOverFlowException(String msg){
		super(msg);
	}
}
class DataUnderFlowException extends RuntimeException{
	DataUnderFlowException(String msg){
		super(msg);
	}
}
class ArrayDemo{
	public static void main(String[] args){
		int arr[] = new int[5];
		Scanner Sc = new Scanner(System.in);
		System.out.println("Enter Integer Value");
		System.out.println("Note : 0 < Integer < 100 ");
		for(int i = 0;i< arr.length;i++){
			int data = Sc.nextInt();
			if(data < 0){
				throw new DataUnderFlowException("Mitra Data Zero Peksha Lahan Aahe");
			}
			if(data > 100){
				throw new DataOverFlowException("Mitra Data 100 Peksha Jast Aahe");
			}
			arr[i] = data;
		}
		for(int i = 0;i<arr.length;i++){
			System.out.println(arr[i]);
		}


	}
}

