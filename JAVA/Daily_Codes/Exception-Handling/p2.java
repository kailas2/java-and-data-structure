class outer{
	void m1(){
		System.out.println("in m1 outer");
		void m2(){
			System.out.println("In m2 outer");
			class inner{
				void innerm1(){
					System.out.println("in m1 inner");
				}
			}
		}
	}
}
class client{
	public static void main(String[] args){
		outer obj = new outer();
		obj.m1();
	}
}
