import java.io.*;
class EqualsMethod{


	static boolean EqualsDemo(String Str1,String Str2){
		char arr1[] = Str1.toCharArray();
		char arr2[] = Str2.toCharArray();
		if(arr1.length != arr2.length){
			return false;
		}
		int i = 0;
		for(char ch : arr1){
			if(ch != arr2[i]){
				return false;
			}
			i++;
		}
		return true;
	}


	public static void main(String args[])throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter String1");
		String Str1 = Br.readLine();
		System.out.println("Enter String2");
		String Str2 = Br.readLine();
		System.out.println(EqualsDemo(Str1,Str2));
		
	}
}
