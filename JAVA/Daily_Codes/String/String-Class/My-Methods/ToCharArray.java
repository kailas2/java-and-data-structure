import java.io.*;
class TOCharArrayMethod{

	static char[] ToCharArrayDemo(String Str1){
		char arr[] = new char[Str1.length()];
		for (int i = 0;i<Str1.length();i++){
			arr[i] = Str1.charAt(i);
		}
		return arr;
	
	}

	public static void main(String[] args) throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter String");
		String Str1 = Br.readLine();
		char ch[] = ToCharArrayDemo(Str1);
		for(char ch1:ch){
			System.out.println(ch1);
		}
	}
}
