class ConcatDemo{
	static String ConcatMethod(String Str1,String Str2){
		return Str1+Str2;
	}
	public static void main(String[] args){
		String Str1 = "Kailas";
		String Str2 = "Dukare";
		String Str3 = ConcatMethod(Str1,Str2);
		System.out.println(Str1 + " + " + Str2 + " = " + Str3);
	}
}

