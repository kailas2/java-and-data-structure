import java.io.*;
class CompareDemo{
	static int IgnoreCaseCompareStringMethod(String Str1,String Str2){
		char CharArr1[] = Str1.toCharArray();
		char CharArr2[] = Str2.toCharArray();
		int Length1 = 0;
		int Length2 = 0;
		for(char ch : CharArr1){
			Length1++;
		}
		for(char ch1 : CharArr2){
			Length2++;
		}
		if(Length1 != Length2){
			return Length1-Length2;
		}
		for(int i = 0;i<Length1;i++){
			if((CharArr1[i] != CharArr2[i]) && (CharArr1[i] >= 65 && CharArr1[i] <= 90)) {
				if(CharArr2[i] != CharArr1[i]+32){
					return CharArr1[i] - CharArr2[i];
				}
			}
			else if((CharArr1[i] != CharArr2[i]) && (CharArr1[i] >= 97 && CharArr1[i] <= 122)){
				if(CharArr2[i] != CharArr1[i] - 32){
					return CharArr1[i] - CharArr2[i];
				}
			}
		}
		return 0;
	}
	public static void main(String[] args)throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter String");
		String Str1 = new String(Br.readLine());
		
		System.out.println("Enter String");
		String Str2 = new String(Br.readLine());

		int Value = IgnoreCaseCompareStringMethod(Str1,Str2);

		System.out.println(Value);


	}
}
