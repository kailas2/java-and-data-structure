class LengthDemo{

	static int LengthMethod(String Str1){
		
		char StringArr[] = Str1.toCharArray();
		int Length = 0;
		for(char ch : StringArr){
			Length++;
		}
		return Length;

	}

	public static void main(String[] args){
		String Str1 = new String("KailasDukare");
		int Length = LengthMethod(Str1);
		System.out.println("Length of " + Str1 + " = " + Length);
	}
}
