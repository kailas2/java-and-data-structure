import java.io.*;
class CharAtIndex{
	static int x = 0;
	static char CharAtIndexMethod(String Str1,int index){
		char CharArr[] = Str1.toCharArray();
		int Length = 0;
                for(char ch : CharArr){
                        Length++;
                }
		return CharArr[index];
	}
	public static void main(String[] args)throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter String");
		String Str1 = Br.readLine();
		System.out.println("Enter Index");
		int index = Integer.parseInt(Br.readLine());
		char Ch = CharAtIndexMethod(Str1,index);
		if(x==0)
			System.out.println("Character at index " + index + " is " + Ch);

	}

}
