import java.io.*;
class CompareDemo{
	static int CompareStringMethod(String Str1,String Str2){
		char CharArr1[] = Str1.toCharArray();
		char CharArr2[] = Str2.toCharArray();
		int Length1 = 0;
		int Length2 = 0;
		for(char ch : CharArr1){
			Length1++;
		}
		for(char ch1 : CharArr2){
			Length2++;
		}
		if(Length1 != Length2){
			return Length1-Length2;
		}
		for(int i = 0;i<Length1;i++){
			if(CharArr1[i] != CharArr2[i]){
				return CharArr1[i] - CharArr2[i];
			}
		}
		return 0;
	}
	public static void main(String[] args)throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter String");
		String Str1 = new String(Br.readLine());
		
		System.out.println("Enter String");
		String Str2 = new String(Br.readLine());

		int Value = CompareStringMethod(Str1,Str2);

		System.out.println(Value);


	}
}
