class StringDemp{
	public static void main(String[] args){
		String Str1 = "kailas";   // Identity hashcode 100

		String Str2 = Str1;       // Identity hash code 100

		String Str3 = new String(Str2);    // Identity hashcode 200

		System.out.println(System.identityHashCode(Str1));
		System.out.println(System.identityHashCode(Str2));
		System.out.println(System.identityHashCode(Str3));
	}
}

