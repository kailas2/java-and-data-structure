class StringDemo{
	public static void main(String[] args){
		String Str1 = "Kailas";    // iHC = 100;
		String Str2 = "Dukare";    // iHC = 200;
		System.out.println(Str1+Str2);   
		String Str3 = "KailasDukare";  // iHC = 300;
		String Str4 = Str1+Str2;       // iHC = 400;     because when + operate on two string there is call for concat method and that makes new object
		System.out.println(System.identityHashCode(Str3));   //  300  
		System.out.println(System.identityHashCode(Str4));   //  400
	}

}

