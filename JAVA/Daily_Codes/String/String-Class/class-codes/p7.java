class StringDemo{
	public static void main(String[] args){
		String Str1 = "Shashi";
		String Str2 = "Bagal";
		
		System.out.println(Str1);
		System.out.println(Str2);
		
		Str1.concat(Str2); // here not change made in Str1 because Str1 is in SCP and new object created in heap section after concat method not in SCP hence value of Str1 did not change
		System.out.println(Str1);

		String Str3 = new String("Kailas");
		String Str4 = new String("Kailas");
		System.out.println(Str3.equals(Str4));
		
		char Str5[] = {'C','2','W'};
		char Str6[] = {'C','2','W'};
		System.out.println(Str5.equals(Str6));

	}

}

