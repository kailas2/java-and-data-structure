class StringDemo{
	public static void main(String[] args){
		String Str1 = "Kanha";                       //  identity hashcode 100  
							     //  Store in String constant pool
							  
		String Str2 = "Kanha";                       //  identity hashcode 100  
							     //  Store in String Constant pool
		
		String Str3 = new String("kanha");           //  identity hashcode 200  
							     //  Store in Heap Section

		String Str4 = new String("kanha");	     //  identity hashcode 300  
							     //  Strore in head section   

		String Str5 = new String("Rahul");           //  identity hashcode 400  
							     //  Store on heap section
		 
		String Str6 = "Rahul";                       //  identity hashcode 500
							     //  Store on SCP


		System.out.println(System.identityHashCode(Str1));
		System.out.println(System.identityHashCode(Str2));
		System.out.println(System.identityHashCode(Str3));
		System.out.println(System.identityHashCode(Str4));
		System.out.println(System.identityHashCode(Str5));
		System.out.println(System.identityHashCode(Str6));

	}
}

