class StringDemo{
	public static void main(String[] args){
		String Str1 = "Core2Web";
		String Str2 = new String("Core2Web");
		String Str4 = new String("C2W");
		char Str3[] = {'C','2','W'};
		System.out.println(Str1);
		System.out.println(Str2);
		System.out.println(Str3);
		
		System.out.println();

		System.out.println(System.identityHashCode(Str2.charAt(0)));
		System.out.println(System.identityHashCode(Str4.charAt(0)));
		System.out.println(System.identityHashCode(Str3[0]));
		System.out.println(System.identityHashCode(Str1.charAt(0)));
		
		System.out.println();
		
		System.out.println(System.identityHashCode(Str1));
		System.out.println(System.identityHashCode(Str2));
		System.out.println(System.identityHashCode(Str3));
		System.out.println(System.identityHashCode(Str4));

	}
}

