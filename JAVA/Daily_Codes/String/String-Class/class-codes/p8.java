class StringDemo{
	public static void main(String[] args){
		String Str1 = "Shashi";
		String Str2 = "Bagal";
		String Str3 = Str1 + Str2 ;     // this Statements calls append method in string 
		String Str4 = Str1.concat(Str2); // this Statement calls concat method in String class
		System.out.println(Str2);
		System.out.println(Str4);
	}
}
