class StringBufferedDemo{
	public static void main(String[] args){
		StringBuffer Str1 = new StringBuffer("Shashi");
		String Str2 = "Bagal";
		
		System.out.println(System.identityHashCode(Str1));
		
		Str1 = Str1.append(Str2);
		
		System.out.println(Str1);
		
		System.out.println(System.identityHashCode(Str1));
	}
}
