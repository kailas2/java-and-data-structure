class StringBufferedDemo{
	public static void main(String[] args){
		StringBuffer Str1 = new StringBuffer("Shashi");
		String Str2 = "Bagal";

		StringBuffer Str3 = new StringBuffer(Str1);
		
		System.out.println(System.identityHashCode(Str1));
		System.out.println(System.identityHashCode(Str3));
		
		Str2 = Str2+Str1;
		
		Str1 = Str1.append(Str2);

			
		System.out.println("Str1 = " + Str1);
		
		System.out.println("Str2 = " + Str2);
		
		System.out.println("Str3 = " + Str3);

		
		System.out.println(System.identityHashCode(Str1));
		System.out.println(System.identityHashCode(Str3));
	}
}
