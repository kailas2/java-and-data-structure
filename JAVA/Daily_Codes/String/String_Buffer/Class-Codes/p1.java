class StringBufferDemo{
	public static void main(String[] args){
		StringBuffer Sb = new StringBuffer();
		System.out.println(Sb); // Blank
		System.out.println(Sb.capacity()); // 16
		
		Sb.append("Shashi");
		System.out.println(Sb);  // Shashi
		System.out.println(Sb.capacity());  // 16
		
		Sb.append("Bagal");
		System.out.println(Sb);   // Shashibagal
		System.out.println(Sb.capacity()); // 16

		Sb.append("Core2Web");
		System.out.println(Sb);   // ShashibagalCore2Web
		System.out.println(Sb.capacity()); // 34


	}

}
