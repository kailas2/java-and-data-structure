class StringDemo{
	public static void main(String[] args){
		String Str1 = "Dukare";
		String Str2 = new String("Kailas");

		String Str3 = Str1;

		Str1 = Str1 + Str2;

		System.out.println("Str1 = " + Str1);
		System.out.println("Str2 = " + Str2);
		System.out.println("Str3 = " + Str3);
	}
}

