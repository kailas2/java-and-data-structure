/*
 *
 * Take no of rows from the user ROWS = 4
	4
      4 3
    4 3 2
  4 3 2 1

*/
class pattern{
	public static void main(String[] args){
		int row = 4;
		int n = row-1;
		int j = row;
		for(int i = 1;i<=row;i++){
			if(i<=n){
				System.out.print("  ");
			}
			else{
				System.out.print(j-- + " ");
			}
			if(i == row && n>0){
				i = 0;
				n--;
				j = row;
				System.out.println();
			}
		}
	}
}

