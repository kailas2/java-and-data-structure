/*
 *
 *Take no of rows from the user ROWS = 4
a b c d
a b c
a b
a
*/
class pattern{
	public static void main(String[] args){
		int row = 4;
		char ch = 'a';
		for(int i = 1;i<=row;i++){
			System.out.print(ch++ + " ");
			if(i == row && row>1){
				i = 0;
				ch = 'a';
				System.out.println();
				row--;
			}
		}
	}
}

