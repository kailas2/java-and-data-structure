/*
 * Take no of rows from the user ROWS = 4
4 3 2 1
C B A
2 1
A
*/
class pattern{
	public static void main(String[] args){
		int row = 4;
		int j = 1;
		int ch = 64+row;
		for(int i = 1;i<=row;i++){
			if(j%2==1){
				System.out.print((row-i+1) + " ");
			}
			else{
				System.out.print((char)(ch--) + " ");
			}
			if(i==row && row>1){
				i = 0;
				row--;
				ch = 64+row;
				System.out.println();
				j++;
			}
		}
	}
}

