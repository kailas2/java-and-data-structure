/*
 * Take no of rows from the user ROWS = 4
      1
    1 2
  1 2 3
1 2 3 4
*/
class pattern{
	public static void main(String[] args){
		int row = 4;
		int n = row-1;
		int k = 1;
		for(int i = 1;i<=row;i++){
				if(i<=n){
					System.out.print("  ");
				}
				else{
					System.out.print(k++ + " ");
				}
				if(i==row && n>0){
					i = 0;
					System.out.println();
					n--;
					k = 1;
				}
		}
	}
}


