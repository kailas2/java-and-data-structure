/*
 Take no of rows from the user ROWS = 4
4 3 2 1
3 2 1
2 1
1
*/
class pattern{
	public static void main(String[] args){
		int row = 4;
		int num = row;
		for(int i = 1;i<=row;i++){
			System.out.print(num + " ");
			num--;
			if(i==row && row>1){
				i = 0;
				row--;
				num = row;
				System.out.println();
			}
		}
	}
}

