/*
 * Take no of rows from the user ROWS = 4
10
I H
7 6 5
D C B A
*/

class pattern{
	public static void main(String[] args){
		int row = 4;
		int num = (row*(row+1))/2;
		int ch = 64+num;
		int n = 1;
		for(int i = 1;i<=row;i++){
			if(n%2==1){
				System.out.print(num + " ");
			}
			else{
				System.out.print((char)(ch)+" ");
			}
			ch--;
			num--;
			if(i==n && n<row){
				i = 0;
				n++;
				System.out.println();
			}

		}
	}
}

