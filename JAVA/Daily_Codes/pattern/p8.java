/*
 *
 * Take no of rows from the user ROWS = 5
1 2 3 4 5
2 3 4 5
3 4 5
4 5
5

*/
class pattern{
	public static void main(String[] args){
		int row = 5;
		int n = 0;
		for(int i = 1;i<=row;i++){
			System.out.print((n+i) + " ");
			if(i == row && row>1){
				row--;
				i = 0;
				System.out.println();
				n++;
			}
		}
	}
}

