/*
 * Take no of rows from the user ROWS = 4
4 3 2 1
4 3 2
4 3
4
*/
class pattern{
	public static void main(String[] args){
		int row = 4;
		int n = row;
		for(int i = 1;i<=row;i++){
			System.out.print(n-i+1 + " ");
			if(i == row && row>1){
				row--;
				System.out.println();
				i = 0;
			}
		}
	}
}

