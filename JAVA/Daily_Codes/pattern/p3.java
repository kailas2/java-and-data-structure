/*
 1
 3 5
 5 7 9
 7 9 11 13
 9 11 13 15 17
*/

class pattern{
	public static void main(String[] args){
		int row = 5;
		int num = 1;
		int cnt = 1;
		int j = 1;
		for(int i = 1;i<=row;i++){
			System.out.print(num + " ");
			num+=2;
			if(j==i){
				num = cnt*2+1;
				i = 0;
				j++;
				System.out.println();
				cnt++;
			}

			if(cnt-1 == row){
				break;
			}
		}
	}
}

