
/*
 
 1  2  3  4  5
 2  4  6  8  10
 3  6  9  12 15
 4  8  12 16 20
 5  10 15 20 25
*/


class pattern{
	public static void main(String[] args){
		int row = 5;
		int col = 5;
		int num = 1;
		for(int i = 1;i<=row;i++){
			System.out.print(num*i + " ");
			if(i==col){
				System.out.println();
				i = 0;
				num++;
			}
			if(num == row+1){
				break;
			}
		}
	}
}
