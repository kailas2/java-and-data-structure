/*
 * Take no of rows from the user ROWS = 4
1 2 3 4
4 5 6
6 7
7
*/
class pattern{
	public static void main(String[] args){
		int row = 4;
		int n = 1;
		for(int i = 1;i<=row;i++){
			System.out.print(n + " ");
			n++;
			if(i == row && row>1){
				row--;
				i = 0;
				System.out.println();
				n--;
			}
		}
	}
}
