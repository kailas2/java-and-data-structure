/*
 *
 * Take no of rows from the user ROWS = 4
       d
     c c
   b b b
 a a a a

*/
class pattern{
	public static void main(String[] args){
		int row = 4;
		int n = row-1;
		for(int i = 1;i<=row;i++){
			if(i<=n){
				System.out.print("  ");
			}
			else{
				System.out.print((char)(97+n) + " ");
			}
			if(i == row && n>0){
				i = 0;
				n--;
				System.out.println();
			}
		}
	}
}




