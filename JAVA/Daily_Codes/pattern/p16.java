/*
 *
           1
         6 2
      10 7 3
   13 11 8 4
15 14 12 9 5

*/

class pattern{
	public static void main(String[] args){
		int row = 5;
		int num = row-1;
		int n = 1;
		int j = 1;
		int cnt = 0;
		for(int i = 1;i<=row;i++){
			if(i<=num){
				System.out.print("\t");
			}
			else{
				System.out.print(n + "\t");
				n = n-i;
			}
			if(i == row && cnt<row-1){
				i = 0;
				System.out.println();
				j = j+row-cnt;
				n = j;
				num--;
				cnt++;
			}
		}
	}
}
