/*
 5  6  15  16  25
 4  7  14  17  24
 3  8  13  18  23
 2  9  12  19  22
 1  10 11  20  21 

*/
class pattern{
       public static void main(String[] args){
		int row = 5;
		int col = 5;
		int x = row;
		int y = 1;
		int j = 0;
		for(int i = 1;i<=row;i++){ 
			if(i%2==1){
				System.out.print(x + " ");
			}
			else{
				System.out.print(y + " ");
			}
			x = x + row;
			y = y + row;
			if(i == row){
				System.out.println();
				i = 0;
				j++;
				x = row-j;
				y = j+1;
			}
			if(j==row){
				break;
			}

		}
       }
}


