/*
 *
 * Take no of rows from the user ROWS = 4
      D
    c D
  B c D
a B c D

*/
class patern{
	public static void main(String[] args){
		int row = 4;
		int n = row-1;
		for(int i = 1;i<=row;i++){
			if(i<=n){
				System.out.print("  ");
			}
			else{
				if(i%2==1){
					System.out.print((char)(96+i)+ " ");
				}
				else{
					System.out.print((char)(64+i)+ " ");
				}
			}
			if(i == row && n>0){
				i = 0;
				n--;
				System.out.println();
			}
		}
	}
}

