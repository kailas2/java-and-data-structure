


class Mythread extends Thread{
	Mythread(ThreadGroup TG,String Str){
		super(TG,Str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
	}
}
class ThreadGroupDemo{
	public static void main(String[] args){
		ThreadGroup PThread = new ThreadGroup("Core2Web");
		Mythread obj1 = new Mythread(PThread,"Python");
		Mythread obj2 = new Mythread(PThread,"Java");
		Mythread obj3 = new Mythread(PThread,"C++");

		obj1.start();
		obj2.start();
		obj3.start();


		ThreadGroup CThread = new ThreadGroup("Incubator");

		Mythread obj4 = new Mythread(CThread,"Flutter");
		Mythread obj5 = new Mythread(CThread,"SprinBoot");

		obj4.start();
		obj5.start();
		
	}
}
