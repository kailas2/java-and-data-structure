class Mythread extends Thread{
	Mythread(ThreadGroup Tg,String Str){
		super(Tg,Str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
		try{
			Thread.sleep(5000);
		}
		catch(InterruptedException IE){
			System.out.println(IE.toString());	
		}

	}
}

class ThreadGroupDemo{

	public static void main(String[] args)throws InterruptedException{
		ThreadGroup PThread = new ThreadGroup("India");

		Mythread obj1 = new Mythread(PThread,"Maharashtra");
		Mythread obj2 = new Mythread(PThread,"Goa");

		obj1.start();
		obj2.start();

		ThreadGroup CThread1 = new ThreadGroup("Pakistan");

		Mythread obj3 = new Mythread(CThread1,"Lahore");
		Mythread obj4 = new Mythread(CThread1,"Karachi");

		obj3.start();
		obj4.start();
		
		
		ThreadGroup CThread2 = new ThreadGroup("Bangladesh");

		Mythread obj5 = new Mythread(CThread2,"Dhaka");
		Mythread obj6 = new Mythread(CThread2,"Mirpur");

		obj5.start();
		obj6.start();

		CThread1.interrupt();

		System.out.println(PThread.activeCount());
		System.out.println(PThread.activeGroupCount());



	}
}
