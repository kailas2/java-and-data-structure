//  Conqurency methods in java
/*
 
   1 = Sleep
   2 = Join
   3 = Yield
 
 */

// Sleep

class Mythread extends Thread{
	public void run(){
		System.out.println(Thread.currentThread());
	}
}

class ThreadDemo{
	public static void main(String[] args)throws InterruptedException{
		System.out.println(Thread.currentThread());
		Mythread obj = new Mythread();
		obj.start();
		Thread.sleep(1000); // Sleep Throws InterruptedException

		Thread.currentThread().setName("Core2Web");
		System.out.println(Thread.currentThread());
	}
}



