class Mythread implements Runnable{
	public void run(){
		System.out.println(Thread.currentThread());

		try{
			Thread.sleep(5000);
		}
		catch(InterruptedException IE){
			System.out.println(IE.toString());
		}
	}
}

class ThreadGroupDemo{
	public static void main(String[] args){
		ThreadGroup PThread = new ThreadGroup("India");
		Mythread obj1 = new Mythread();
		Mythread obj2 = new Mythread();

		Thread T1 = new Thread(PThread,obj1,"Maharashtra");
		Thread T2 = new Thread(PThread,obj2,"Goa");

		T1.start();
		T2.start();

	}
}
