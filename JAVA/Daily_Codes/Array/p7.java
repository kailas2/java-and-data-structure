// Take a no from user and create an array of that size and put natural no in array
import java.util.Scanner;
class Array{
	public static void main(String[] args){
		System.out.println("Enter no");
		int arr1[] = {10,20,30,40,50};
		char arr2[] = {'A','B','C'};
		float arr3[] = {10.0f,11.0f,12.0f};
		boolean arr4[] = {true,false,true};

		for(int i = 0;i<arr1.length;i++){
			System.out.println(arr1[i]);
		}
		for(int i = 0;i<arr2.length;i++){
			System.out.println(arr2[i]);
		}

		for(int i = 0;i<arr3.length;i++){
			System.out.println(arr3[i]);
		}
		for(int i = 0;i<=arr4.length;i++){     // this loop give an Exeception error b.ArrayIndexOutOfBoundsException  because we are ascessing inde							   x not present in array
						       // this error comes at runtime because memory was given to an array at runtime and dynamically memory							   // allocation for array at heap section 
			System.out.println(arr4[i]);
		}
	}
}
		
