import java.io.*;
class jaggedArray{
	public static void main(String[] args) throws IOException{
		int arr[][] = new int[3][];
		arr[0] = new int[3];
		arr[1] = new int[2];
		arr[2] = new int[1];
		for(int[] i : arr){
			for(int j : i){
				System.out.print(j + " ");
			}
			System.out.println();
		}
		/*

		  0 0 0
		  0 0
		  0

		*/

		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		int k = 0;
		for(int[] i : arr){
			System.out.println("Enter Values in Row");
			int l = 0;
			for(int j : i){
				arr[k][l] = Integer.parseInt(Br.readLine());
				l++;
			}
			k++;
		}
		for(int[] i : arr){
			for(int j : i){
				System.out.print(j + " ");
			}
			System.out.println();
		}

	}
}

