class Null_Pointer_Exception{
	public static void main(String[] args){
		int arr1[][] = {{},{},{}};
		int arr2[][] = new int[3][];
		System.out.println(arr1.length);
		System.out.println(arr1[0].length);
		System.out.println(arr2.length);
		System.out.println(arr2[0].length);   // Null pointer Exception 
						      // Because in first array we declare row and coloums 
						      // in second array only rows are declared 
	}

}

