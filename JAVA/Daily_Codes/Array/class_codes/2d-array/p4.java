class TwoDarray{
	public static void main(String[] args){
		int arr[][] = new int[2][2];
		arr[0][0] = 10;
		arr[0][1] = 10;
		arr[1][0] = 10;
		arr[1][1] = 10;
		System.out.println(arr[1][1]);     // 10
		System.out.println(arr[0]);        // [ I@ address
		System.out.println(arr[1]);        // [ I@ address
		System.out.println(arr);	   // [[ I@ address
		System.out.println(System.identityHashCode(arr[0][0]));    // hascode of all elements in 2d array is same because of java memeory management id if any value is between
									   // -128 to 127    
		System.out.println(System.identityHashCode(arr[0][1]));     
		System.out.println(System.identityHashCode(arr[1][0]));     
		System.out.println(System.identityHashCode(arr[1][1]));  

  	}
}

