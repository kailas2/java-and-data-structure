class ArrayDemo{
	public static void main(String[] args){
		int arr[] = new int[4];  // This is the Declaration of an array
		// Putting values in array by manually
		arr[0] = 10;
		arr[1] = 10;
		arr[2] = 10;
		arr[3] = 10;

		int arr1[] = {20,20,20,20}; // Assign an array directly with data



		int arr2[] = new int[]{30,30,30,30};   // Assign an array directly with data
		

		int arr3[] = new int[4]{40,40,40,40};   // This is Invalid Statement  Size and data at one time not allowed in java 

		
	}


}

