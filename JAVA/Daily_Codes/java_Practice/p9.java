/* 7. WAP to take input as a string and trim the string ( try without using inbuilt method) . */

import java.io.*;
class Trim{
	static String TrimString(String Str){
		String Str2 = "";
		char CharArr[] = Str.toCharArray();
		int flag = 0;
		int count = 0;
		for(char ch : CharArr){
			
			if(ch != 32){
				while(count > 0){
					Str2 = Str2 + " ";
					count--;
				}
				Str2 = Str2 + ch;
				flag = 1;
					
			}
			else{
				if(flag == 1){	
					count++;
				}
			}
		}

		return Str2;
	}
	public static void main(String[] args)throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter String");
		String Str = Br.readLine();
		
		String Str2 = TrimString(Str);

		System.out.println(Str2);
	}
}

