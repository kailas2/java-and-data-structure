/*  4. WAP to print square roots of numbers in given range */

import java.io.*;
class Squareroot{

	static void SquareRoots(int Start,int End){
		for(int i = Start;i<=End;i++){
			if(i == 0 || i == 1){
				System.out.println("Square Root Of "+ i + " = " + i + " ");
			}
			else{
				int Sq = 1;
				int val = 1;

				while(val <= i){
					Sq++;
					val = Sq*Sq;
				}
				System.out.println("Square Root Of "+ i + " = " + (Sq-1) + " ");

			}
		}
	}
	public static void main(String[] args) throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Start");
		int Start = Integer.parseInt(Br.readLine());
		
		System.out.println("Enter End");
		int End = Integer.parseInt(Br.readLine());

		SquareRoots(Start,End);
		
	}
}
