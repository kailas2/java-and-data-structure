/* 2. WAP to take input from user to check whether given string is palindrome string or not. */


import java.io.*;
class PalindromString{
    static int StringLength(char[] CharArr){
	
	int Length = 0;
	for(char ch:CharArr){
	    Length++;
	}
	return Length;
	}

    
    static boolean CheckPalindromString(String Str){
		
	char CharArr[] = Str.toCharArray();
		
	int Length = StringLength(CharArr)-1;

	for(int i = 0;i<Length;i++){
	    
	    if(CharArr[i] != CharArr[Length]){
		return false;
	    }
	    Length--;
	}
	return true;
    }
    public static void main(String[] args)throws IOException{
	
	BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
	
	System.out.println("Enter String");
	
	String Str = Br.readLine();
	
	boolean Var = CheckPalindromString(Str);
	
	if(Var == true){
	    System.out.println("Palindrom String");
	}
	else{
	    System.out.println("Not palindrom String");
	}
    }
}

