/* 6. WAP to take input using string class and count the vowels and consonants in the given string. */

import java.io.*;

class VovelsAndConsonants{

	static int[] Count(String Str){
		char CharArr[] = Str.toCharArray();
		int Arr[] = new int[2];
		for(char ch : CharArr){
			if(ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U' || ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u'){
				Arr[0]++;
			}
			else{
				Arr[1]++;
			}
		}
		
		return Arr;
	}
	public static void main(String[] args)throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter String");
		String Str = Br.readLine();
		
		int arr[] = Count(Str);

		System.out.println("Vowels Count = " + arr[0]);
		System.out.println("Consonants Count = " + arr[1]);

	}

}

