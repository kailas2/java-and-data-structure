/*
 
 5. WAP to print the pattern
Row =3
*
* *
* * *
* *
*

*/
import java.io.*;
class Pattern{
	public static void main(String[] args)throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Rows");
		int Row = Integer.parseInt(Br.readLine());
		for(int i = 1;i<=Row;i++){
			for(int j = 0;j<i;j++){
				
				System.out.print("* ");
			}
			
			System.out.println();
		}
		for(int i = Row-1;i>0;i--){
			for(int j = 0;j<i;j++){
				
				System.out.print("* ");
			}
			
			System.out.println();
		}

	}
}

