/*
3.WAP to take 2 input  strings from user and check whether they are Anagram strings or not
   ( string with exactly the same length quantity of each character in it, in any order ) 
Ex : ashish & shashi  */
import java.io.*;
class AnagramString{
	static int StringLength(String Str){
		int Length = 0;
		char CharArr[] = Str.toCharArray();
		for(char ch:CharArr){
			Length++;
		}
		
		return Length;
        }
	static boolean CheckAnagram(String Str1,String Str2){
		int Length1 = StringLength(Str1);
                int Length2 = StringLength(Str2);
                int arr[] = new int[128];
                if(Length1 == Length2){
                        char CharArr1[] = Str1.toCharArray();
                        for(char ch : CharArr1){
                                arr[ch]++;
                        }
                        char CharArr2[] = Str2.toCharArray();
                        for(char ch : CharArr2){
                                arr[ch]--;
                        }
			for(char ch : CharArr1){
                                if(arr[ch] != 0){
                                        return false;
                                }
                        }
                        for(char ch : CharArr2){
                                if(arr[ch] != 0){
                                        return false;
                                }
                        }
                        return true;
                }
                return false;
	}
	public static void main(String[] args)throws IOException{
		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter String");
		String Str1 = Br.readLine();
		System.out.println("Enter String");
		String Str2 = Br.readLine();
		boolean Val = CheckAnagram(Str1,Str2);
		if(Val == true){
			System.out.println("Anagram String");
		}
		else{
			System.out.println("Not An Anagram String");
		}
	}
}

