/*
 
 1.WAP to take input from user in 2D array and skip the diagonal elements while printing the array.

 Ex.   1  2  3
       4  5  6 
       7  8  9
       
Output- 2,3,4,6,7,8

*/
import java.io.*;
class TwoD_Demo{
	public static void main(String[] args)throws IOException{

		BufferedReader Br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Rows");
		int Rows = Integer.parseInt(Br.readLine());

		System.out.println("Enter Columns");
		int Columns = Integer.parseInt(Br.readLine());

		int Arr[][] = new int[Rows][Columns];
		System.out.println("Enter data in array");

		for(int i = 0;i<Rows;i++){
			for(int j = 0;j<Columns;j++){
				Arr[i][j] = Integer.parseInt(Br.readLine());
			}
		}
		for(int i = 0;i<Rows;i++){
			for(int j = 0;j<Columns;j++){
				if(i!=j)
					System.out.print(Arr[i][j] + " ");

			}
		}


	}
}

